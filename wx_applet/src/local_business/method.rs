use crate::common::{wx_post_json, TransportCapacityResponse, WxResponse};

use super::param::{
    AbnormalConfirmReq, AddLocalOrderReq, AddLocalOrderRes, AddTipsReq, BindAccountReq,
    BindAccountRes, BindLocalAccountReq, BindLocalAccountRes, CancelLocalOrderReq,
    CancelLocalOrderRes, GetLocalOrderReq, GetLocalOrderRes, ImmeDeliveryReq, ImmeDeliveryRes,
    MockUpdateOrderReq, MockUpdateOrderRes, OpenDeliveryReq, OpenDeliveryRes, PreAddOrderReq,
    PreAddOrderRes, PreCancelOrderReq, PreCancelOrderRes, ReOrderReq, ReOrderRes,
    RealMockUpdateOrderReq, RealMockUpdateOrderRes, UpdateOrderReq,
};

pub async fn get_all_imme_delivery<T>(
    token: T,
    req: &ImmeDeliveryReq,
) -> WxResponse<ImmeDeliveryRes>
where
    T: AsRef<str>,
{
    wx_post_json(
        "https://api.weixin.qq.com/cgi-bin/express/local/business/delivery/getall",
        token,
        req,
    )
    .await
}

pub async fn pre_add_order<T>(token: T, req: &PreAddOrderReq) -> WxResponse<PreAddOrderRes>
where
    T: AsRef<str>,
{
    wx_post_json(
        "https://api.weixin.qq.com/cgi-bin/express/local/business/order/pre_add",
        token,
        req,
    )
    .await
}

pub async fn get_bind_account<T>(token: T, req: &BindAccountReq) -> WxResponse<BindAccountRes>
where
    T: AsRef<str>,
{
    wx_post_json(
        "https://api.weixin.qq.com/cgi-bin/express/local/business/shop/get",
        token,
        req,
    )
    .await
}

pub async fn pre_cancel_order<T>(token: T, req: &PreCancelOrderReq) -> WxResponse<PreCancelOrderRes>
where
    T: AsRef<str>,
{
    wx_post_json(
        "https://api.weixin.qq.com/cgi-bin/express/local/business/order/precancel",
        token,
        req,
    )
    .await
}

pub async fn open_delivery<T>(token: T, req: &OpenDeliveryReq) -> WxResponse<OpenDeliveryRes>
where
    T: AsRef<str>,
{
    wx_post_json(
        "https://api.weixin.qq.com/cgi-bin/express/local/business/open",
        token,
        req,
    )
    .await
}

pub async fn bind_local_account<T>(
    token: T,
    req: &BindLocalAccountReq,
) -> WxResponse<BindLocalAccountRes>
where
    T: AsRef<str>,
{
    wx_post_json(
        "https://api.weixin.qq.com/cgi-bin/express/local/business/shop/add",
        token,
        req,
    )
    .await
}

pub async fn re_order<T>(token: T, req: &ReOrderReq) -> WxResponse<ReOrderRes>
where
    T: AsRef<str>,
{
    wx_post_json(
        "https://api.weixin.qq.com/cgi-bin/express/local/business/order/readd",
        token,
        req,
    )
    .await
}

pub async fn real_mock_update_order<T>(
    token: T,
    req: &RealMockUpdateOrderReq,
) -> WxResponse<RealMockUpdateOrderRes>
where
    T: AsRef<str>,
{
    wx_post_json(
        "https://api.weixin.qq.com/cgi-bin/express/local/business/realmock_update_order",
        token,
        req,
    )
    .await
}

pub async fn mock_update_order<T>(
    token: T,
    req: &MockUpdateOrderReq,
) -> WxResponse<MockUpdateOrderRes>
where
    T: AsRef<str>,
{
    wx_post_json(
        "https://api.weixin.qq.com/cgi-bin/express/local/business/test_update_order",
        token,
        req,
    )
    .await
}

pub async fn get_local_order<T>(token: T, req: &GetLocalOrderReq) -> WxResponse<GetLocalOrderRes>
where
    T: AsRef<str>,
{
    wx_post_json(
        "https://api.weixin.qq.com/cgi-bin/express/local/business/order/get",
        token,
        req,
    )
    .await
}

pub async fn abnormal_confirm<T>(
    token: T,
    req: &AbnormalConfirmReq,
) -> WxResponse<TransportCapacityResponse>
where
    T: AsRef<str>,
{
    wx_post_json(
        "https://api.weixin.qq.com/cgi-bin/express/local/business/order/confirm_return",
        token,
        req,
    )
    .await
}

pub async fn cancel_local_order<T>(
    token: T,
    req: &CancelLocalOrderReq,
) -> WxResponse<CancelLocalOrderRes>
where
    T: AsRef<str>,
{
    wx_post_json(
        "https://api.weixin.qq.com/cgi-bin/express/local/business/order/cancel",
        token,
        req,
    )
    .await
}

pub async fn add_tips<T>(token: T, req: &AddTipsReq) -> WxResponse<TransportCapacityResponse>
where
    T: AsRef<str>,
{
    wx_post_json(
        "https://api.weixin.qq.com/cgi-bin/express/local/business/order/addtips",
        token,
        req,
    )
    .await
}

pub async fn add_local_order<T>(token: T, req: &AddLocalOrderReq) -> WxResponse<AddLocalOrderRes>
where
    T: AsRef<str>,
{
    wx_post_json(
        "https://api.weixin.qq.com/cgi-bin/express/local/business/order/add",
        token,
        req,
    )
    .await
}

pub async fn update_order<T>(
    token: T,
    req: &UpdateOrderReq,
) -> WxResponse<TransportCapacityResponse>
where
    T: AsRef<str>,
{
    wx_post_json(
        "https://api.weixin.qq.com/cgi-bin/express/local/delivery/update_order",
        token,
        req,
    )
    .await
}
