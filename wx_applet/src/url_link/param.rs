use serde::{Deserialize, Serialize};

use crate::common::EnvVision;

#[derive(Clone, Debug, Serialize)]
pub struct UrlLinkReq {
    path: Option<String>,
    query: Option<String>,
    expire_type: Option<u8>,
    expire_time: Option<u8>,
    expire_interval: Option<u8>,
    cloud_base: Option<CloudBase>,
    env_version: Option<EnvVision>,
}

#[derive(Clone, Debug, Serialize)]
pub struct CloudBase {
    pub env: String,
    pub domain: Option<String>,
    pub path: Option<String>,
    pub query: Option<String>,
    pub resource_appid: Option<String>,
}

#[derive(Clone, Debug, Deserialize)]
pub struct UrlLinkRes {
    pub url_link: String,
}

#[derive(Clone, Debug, Serialize)]
pub struct QueryUrlLink {
    pub url_link: String,
}

#[derive(Clone, Debug, Deserialize)]
pub struct QueryUrlLinkRes {
    pub url_link_info: UrlLinkInfo,
    pub visit_openid: Option<String>,
}

#[derive(Clone, Debug, Deserialize)]
pub struct UrlLinkInfo {
    pub appid: String,
    pub path: Option<String>,
    pub query: Option<String>,
    pub create_time: u8,
    pub expire_time: u8,
    pub env_vsesion: EnvVision,
}

#[derive(Clone, Debug, Serialize, Default)]
pub struct ShortLinkReq {
    page_url: String,
    page_title: Option<String>,
    is_permanent: Option<bool>,
}

impl ShortLinkReq {
    pub fn new<T>(page_url: T) -> Self
    where
        T: AsRef<str>,
    {
        Self {
            page_url: page_url.as_ref().to_owned(),
            ..Default::default()
        }
    }
}

impl ShortLinkReq {
    pub fn builder() -> ShortLinkReqBuilder {
        ShortLinkReqBuilder::default()
    }
}

#[derive(Clone, Debug, Serialize, Default)]
pub struct ShortLinkReqBuilder {
    page_url: String,
    page_title: Option<String>,
    is_permanent: Option<bool>,
}

impl ShortLinkReqBuilder {
    pub fn page_url<T>(mut self, url: T) -> Self
    where
        T: AsRef<str>,
    {
        self.page_url = url.as_ref().to_owned();
        self
    }

    pub fn page_title<T>(mut self, title: T) -> Self
    where
        T: AsRef<str>,
    {
        self.page_title = Some(title.as_ref().to_owned());
        self
    }

    pub fn permanent(mut self, permanent: bool) -> Self {
        self.is_permanent = Some(permanent);
        self
    }

    pub fn build(self) -> ShortLinkReq {
        ShortLinkReq {
            page_url: self.page_url,
            page_title: self.page_title,
            is_permanent: self.is_permanent,
        }
    }
}

#[derive(Clone, Debug, Deserialize)]
pub struct ShortLinkRes {
    pub link: String,
}
