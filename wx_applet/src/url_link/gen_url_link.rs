use reqwest::Client;

use super::param::{
    QueryUrlLink, QueryUrlLinkRes, ShortLinkReq, ShortLinkRes, UrlLinkReq, UrlLinkRes,
};
use crate::common::Response;

pub async fn gen_url_link<T>(
    token: T,
    req: &UrlLinkReq,
) -> Result<Response<UrlLinkRes>, reqwest::Error>
where
    T: AsRef<str>,
{
    let url = format!(
        "https://api.weixin.qq.com/wxa/generate_urllink?access_token={}",
        token.as_ref()
    );
    let client = Client::new();
    client
        .post(url)
        .json(req)
        .send()
        .await?
        .json::<Response<UrlLinkRes>>()
        .await
}

pub async fn query_url_link<T>(
    token: T,
    req: &QueryUrlLink,
) -> Result<Response<QueryUrlLinkRes>, reqwest::Error>
where
    T: AsRef<str>,
{
    let url = format!(
        "https://api.weixin.qq.com/wxa/query_urllink?access_token={}",
        token.as_ref()
    );
    let client = Client::new();
    client
        .post(url)
        .json(req)
        .send()
        .await?
        .json::<Response<QueryUrlLinkRes>>()
        .await
}

pub async fn generate_short_link<T>(
    token: T,
    req: &ShortLinkReq,
) -> Result<Response<ShortLinkRes>, reqwest::Error>
where
    T: AsRef<str>,
{
    let url = format!(
        "https://api.weixin.qq.com/wxa/genwxashortlink?access_token={}",
        token.as_ref()
    );
    let client = Client::new();
    client
        .post(url)
        .json(req)
        .send()
        .await?
        .json::<Response<ShortLinkRes>>()
        .await
}
