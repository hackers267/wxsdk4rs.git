use crate::common::wx_get;
use crate::common::wx_post_json;
use crate::common::WxResponse;

use super::AddOrderReq;
use super::AddOrderRes;
use super::AllAccountRes;
use super::AllDeliveryRes;
use super::BatchGetOrderReq;
use super::BatchGetOrderRes;
use super::ContractReq;
use super::ContractRes;
use super::GetOrderReq;
use super::GetOrderRes;
use super::OrderPathReq;
use super::OrderPathRes;
use super::OrderReq;
use super::OrderRes;
use super::PreviewTemplateReq;
use super::PreviewTemplateRes;
use super::Printer;
use super::QuotaReq;
use super::QuotaRes;
use super::TestUpdateOrderReq;
use super::TestUpdateOrderRes;
use super::UpdateBusinessReq;
use super::UpdateBusinessRes;
use super::UpdatePathReq;
use super::UpdatePathRes;
use super::UpdatePrinterReq;
use super::UpdatePrinterRes;
use super::{BindAccountReq, BindAccountRes};

pub async fn bind_account<T>(token: T, req: &BindAccountReq) -> WxResponse<BindAccountRes>
where
    T: AsRef<str>,
{
    wx_post_json(
        "https://api.weixin.qq.com/cgi-bin/express/business/account/bind",
        token,
        req,
    )
    .await
}

pub async fn all_account<T>(token: T) -> WxResponse<AllAccountRes>
where
    T: AsRef<str>,
{
    wx_get(
        "https://api.weixin.qq.com/cgi-bin/express/business/account/getall",
        token,
    )
    .await
}

pub async fn all_delivery<T>(token: T) -> WxResponse<AllDeliveryRes>
where
    T: AsRef<str>,
{
    wx_get(
        "https://api.weixin.qq.com/cgi-bin/express/business/delivery/getall",
        token,
    )
    .await
}

pub async fn cancel_order<T>(token: T, req: &OrderReq) -> WxResponse<OrderRes>
where
    T: AsRef<str>,
{
    wx_post_json(
        "https://api.weixin.qq.com/cgi-bin/express/business/order/cancel",
        token,
        req,
    )
    .await
}

pub async fn update_printer<T>(token: T, req: &UpdatePrinterReq) -> WxResponse<UpdatePrinterRes>
where
    T: AsRef<str>,
{
    wx_post_json(
        "https://api.weixin.qq.com/cgi-bin/express/business/printer/update",
        token,
        req,
    )
    .await
}

pub async fn get_quota<T>(token: T, req: &QuotaReq) -> WxResponse<QuotaRes>
where
    T: AsRef<str>,
{
    wx_post_json(
        "https://api.weixin.qq.com/cgi-bin/express/business/quota/get",
        token,
        req,
    )
    .await
}

pub async fn get_order<T>(token: T, req: &GetOrderReq) -> WxResponse<GetOrderRes>
where
    T: AsRef<str>,
{
    wx_post_json(
        "https://api.weixin.qq.com/cgi-bin/express/business/order/get",
        token,
        req,
    )
    .await
}

pub async fn test_update_order<T>(
    token: T,
    req: &TestUpdateOrderReq,
) -> WxResponse<TestUpdateOrderRes>
where
    T: AsRef<str>,
{
    wx_post_json(
        "https://api.weixin.qq.com/cgi-bin/express/business/test_update_order",
        token,
        req,
    )
    .await
}

pub async fn get_printer<T>(token: T) -> WxResponse<Printer>
where
    T: AsRef<str>,
{
    wx_get(
        "https://api.weixin.qq.com/cgi-bin/express/business/printer/getall",
        token,
    )
    .await
}

pub async fn get_order_path<T>(token: T, req: &OrderPathReq) -> WxResponse<OrderPathRes>
where
    T: AsRef<str>,
{
    wx_post_json(
        "https://api.weixin.qq.com/cgi-bin/express/business/path/get",
        token,
        req,
    )
    .await
}

pub async fn batch_get_order<T>(token: T, req: &BatchGetOrderReq) -> WxResponse<BatchGetOrderRes>
where
    T: AsRef<str>,
{
    wx_post_json(
        "https://api.weixin.qq.com/cgi-bin/express/business/order/batchget",
        token,
        req,
    )
    .await
}

pub async fn add_order<T>(token: T, req: &AddOrderReq) -> WxResponse<AddOrderRes>
where
    T: AsRef<str>,
{
    wx_post_json(
        "https://api.weixin.qq.com/cgi-bin/express/business/order/add",
        token,
        req,
    )
    .await
}

pub async fn update_business<T>(token: T, req: &UpdateBusinessReq) -> WxResponse<UpdateBusinessRes>
where
    T: AsRef<str>,
{
    wx_post_json(
        "https://api.weixin.qq.com/cgi-bin/express/delivery/service/business/update",
        token,
        req,
    )
    .await
}

pub async fn update_path<T>(token: T, req: &UpdatePathReq) -> WxResponse<UpdatePathRes>
where
    T: AsRef<str>,
{
    wx_post_json(
        "https://api.weixin.qq.com/cgi-bin/express/delivery/path/update",
        token,
        req,
    )
    .await
}

pub async fn preview_template<T>(
    token: T,
    req: &PreviewTemplateReq,
) -> WxResponse<PreviewTemplateRes>
where
    T: AsRef<str>,
{
    wx_post_json(
        "https://api.weixin.qq.com/cgi-bin/express/delivery/template/preview",
        token,
        req,
    )
    .await
}

pub async fn get_contract<T>(token: T, req: &ContractReq) -> WxResponse<ContractRes>
where
    T: AsRef<str>,
{
    wx_post_json(
        "https://api.weixin.qq.com/cgi-bin/express/delivery/contact/get",
        token,
        req,
    )
    .await
}
