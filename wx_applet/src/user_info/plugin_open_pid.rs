use reqwest::Client;
use serde::{Deserialize, Serialize};

use crate::common::Response;

#[derive(Clone, Debug, Deserialize)]
pub struct PluginOpenPidRes {
    pub openpid: String,
}

#[derive(Clone, Debug, Serialize)]
pub struct PluginOpenPidReq {
    code: String,
}

impl PluginOpenPidReq {
    pub fn new<T>(code: T) -> Self
    where
        T: AsRef<str>,
    {
        Self {
            code: code.as_ref().to_owned(),
        }
    }
}

type Res = Response<PluginOpenPidRes>;

pub async fn get_plugin_open_pid<T>(
    token: T,
    params: &PluginOpenPidReq,
) -> Result<Res, reqwest::Error>
where
    T: AsRef<str>,
{
    let url = format!(
        "https://api.weixin.qq.com/wxa/getpluginopenpid?access_token={}",
        token.as_ref()
    );
    let client = Client::new();
    client
        .post(url)
        .json(params)
        .send()
        .await?
        .json::<Res>()
        .await
}
