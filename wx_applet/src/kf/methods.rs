use bytes::Bytes;
use reqwest::Client;

use crate::common::{Response, WxErr};

use super::param::{TempMedia, Typing};

pub async fn get_temp_media<T>(token: T, req: &TempMedia) -> Result<Response<Bytes>, reqwest::Error>
where
    T: AsRef<str>,
{
    let url = format!(
        "https://api.weixin.qq.com/cgi-bin/media/get?access_token={}",
        token.as_ref()
    );
    let client = Client::new();
    let res = client.post(url).json(req).send().await?;
    if res.status().is_success() {
        Ok(Response::Ok(res.bytes().await?))
    } else {
        Ok(Response::Err(res.json::<WxErr>().await?))
    }
}
pub async fn set_typing<T>(token: T, req: &Typing) -> Result<Response<()>, reqwest::Error>
where
    T: AsRef<str>,
{
    let url = format!(
        "https://api.weixin.qq.com/cgi-bin/message/custom/business/typing?access_token={}",
        token.as_ref()
    );
    let client = Client::new();
    client
        .post(url)
        .json(req)
        .send()
        .await?
        .json::<Response<()>>()
        .await
}
