use serde::Serialize;

#[derive(Clone, Debug, Serialize)]
pub struct TempMedia {
    media_id: String,
}

impl TempMedia {
    pub fn new<T>(id: T) -> Self
    where
        T: AsRef<str>,
    {
        Self {
            media_id: id.as_ref().to_owned(),
        }
    }
}

#[derive(Clone, Debug, Serialize)]
pub struct Typing {
    touser: String,
    command: String,
}

impl Typing {
    pub fn typing<T>(touser: T) -> Self
    where
        T: AsRef<str>,
    {
        Self {
            touser: touser.as_ref().to_owned(),
            command: "Typing".to_owned(),
        }
    }

    pub fn cancel_typing<T>(touser: T) -> Self
    where
        T: AsRef<str>,
    {
        Self {
            touser: touser.as_ref().to_owned(),
            command: "CancelTyping".to_owned(),
        }
    }
}
