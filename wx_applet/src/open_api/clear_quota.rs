use reqwest::Client;
use serde::{Deserialize, Serialize};

use crate::common::Response;

#[derive(Clone, Debug, Serialize)]
pub struct ClearQuoteReq {
    appid: String,
}

impl ClearQuoteReq {
    pub fn new<T>(appid: T) -> Self
    where
        T: AsRef<str>,
    {
        Self {
            appid: appid.as_ref().to_owned(),
        }
    }
}

#[derive(Clone, Debug, Deserialize)]
pub struct ClearQuoteRes;

pub async fn clear_quote<T>(
    token: T,
    quote: &ClearQuoteReq,
) -> Result<Response<ClearQuoteRes>, reqwest::Error>
where
    T: AsRef<str>,
{
    let url = format!(
        "https://api.weixin.qq.com/cgi-bin/clear_quota?access_token={}",
        token.as_ref()
    );
    let client = Client::new();
    client
        .post(url)
        .json(quote)
        .send()
        .await?
        .json::<Response<ClearQuoteRes>>()
        .await
}

#[derive(Clone, Debug, Serialize)]
pub struct ClearQuoteByAppSecretReq {
    appid: String,
    appsecret: String,
}

impl ClearQuoteByAppSecretReq {
    pub fn new<T>(appid: T, secret: T) -> Self
    where
        T: AsRef<str>,
    {
        Self {
            appid: appid.as_ref().to_owned(),
            appsecret: secret.as_ref().to_owned(),
        }
    }
}

pub async fn clear_quote_by_appsecret(
    req: &ClearQuoteByAppSecretReq,
) -> Result<Response<ClearQuoteRes>, reqwest::Error> {
    let url = "";
    let client = Client::new();
    client
        .post(url)
        .json(req)
        .send()
        .await?
        .json::<Response<ClearQuoteRes>>()
        .await
}
