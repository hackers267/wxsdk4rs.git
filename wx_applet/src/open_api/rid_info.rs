use reqwest::Client;
use serde::{Deserialize, Serialize};

use crate::common::Response;

#[derive(Clone, Debug, Serialize)]
pub struct RidInfoReq {
    rid: String,
}

impl RidInfoReq {
    pub fn new<T>(rid: T) -> Self
    where
        T: AsRef<str>,
    {
        Self {
            rid: rid.as_ref().to_owned(),
        }
    }
}
#[derive(Clone, Debug, Deserialize)]
pub struct RidInfoRes {
    pub request: Request,
}

#[derive(Clone, Debug, Deserialize)]
pub struct Request {
    pub invoke_time: u32,
    pub cost_in_ms: u32,
    pub request_url: String,
    pub request_body: String,
    pub response_body: String,
    pub client_ip: String,
}
pub async fn get_rid_info<T>(
    token: T,
    info: &RidInfoReq,
) -> Result<Response<RidInfoRes>, reqwest::Error>
where
    T: AsRef<str>,
{
    let url = format!(
        "https://api.weixin.qq.com/cgi-bin/openapi/rid/get?access_token={}",
        token.as_ref()
    );
    let client = Client::new();
    client
        .post(url)
        .json(info)
        .send()
        .await?
        .json::<Response<RidInfoRes>>()
        .await
}
