use crate::common::Response;
use reqwest::Client;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Serialize)]
pub struct Code2Seccion {
    appid: String,
    secret: String,
    js_code: String,
    grant_type: String,
}

impl Code2Seccion {
    pub fn new<T>(appid: T, secret: T, code: T) -> Self
    where
        T: AsRef<str>,
    {
        Self {
            appid: appid.as_ref().to_owned(),
            secret: secret.as_ref().to_owned(),
            js_code: code.as_ref().to_owned(),
            grant_type: "authorization_code".to_owned(),
        }
    }
}

#[derive(Clone, Debug, Deserialize)]
pub struct Code2SessionRes {
    pub openid: String,
    pub session_key: String,
    pub unionid: Option<String>,
}

type Res = Response<Code2SessionRes>;

pub async fn code2session(code: &Code2Seccion) -> Result<Res, reqwest::Error> {
    let url = "https://api.weixin.qq.com/sns/jscode2session";
    let client = Client::new();
    client
        .get(url)
        .query(code)
        .send()
        .await?
        .json::<Res>()
        .await
}
