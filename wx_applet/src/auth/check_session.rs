use crate::common::WxErr;
use reqwest::Client;
use serde::Serialize;

#[derive(Debug, Clone, Serialize)]
pub struct CheckSessionKeyReq {}

/// 校验服务器所保存的登录态 session_key 是否合法
pub async fn check_session_key(params: &CheckSessionKeyReq) -> Result<WxErr, reqwest::Error> {
    let url = "";
    let client = Client::new();
    client
        .get(url)
        .json(params)
        .send()
        .await?
        .json::<WxErr>()
        .await
}
