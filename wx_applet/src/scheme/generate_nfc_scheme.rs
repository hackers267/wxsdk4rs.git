use reqwest::Client;
use serde::{Deserialize, Serialize};

use crate::common::Response;

use super::param::JumpWxa;

#[derive(Clone, Debug, Serialize, Default)]
pub struct NfcSchemeReq<'a> {
    jump_wxa: Option<&'a JumpWxa>,
    model_id: String,
    sn: Option<String>,
}

impl<'a> NfcSchemeReq<'a> {
    pub fn new<T>(id: T) -> Self
    where
        T: AsRef<str>,
    {
        Self {
            model_id: id.as_ref().to_owned(),
            ..Default::default()
        }
    }
}

#[derive(Clone, Debug, Deserialize)]
pub struct NfcSchemeRes {
    pub openlink: String,
}

pub async fn generate_nfc_scheme<'a, T>(
    token: T,
    req: &NfcSchemeReq<'a>,
) -> Result<Response<NfcSchemeRes>, reqwest::Error>
where
    T: AsRef<str>,
{
    let url = format!(
        "https://api.weixin.qq.com/wxa/generatescheme?access_token={}",
        token.as_ref()
    );
    let client = Client::new();
    client
        .post(url)
        .json(req)
        .send()
        .await?
        .json::<Response<NfcSchemeRes>>()
        .await
}
