use reqwest::Client;
use serde::{Deserialize, Serialize};

use crate::common::Response;

use super::param::JumpWxa;

#[derive(Clone, Debug, Serialize)]
pub struct GenerateSchemeReq<'a> {
    jump_wxa: Option<&'a JumpWxa>,
    expire_time: Option<u32>,
    expire_type: Option<String>,
    expire_interval: Option<String>,
}

#[derive(Clone, Debug, Deserialize)]
pub struct GenerateSchemeRes {
    pub openlink: String,
}

pub async fn generate_scheme<'a, T>(
    token: T,
    req: &GenerateSchemeReq<'a>,
) -> Result<Response<GenerateSchemeRes>, reqwest::Error>
where
    T: AsRef<str>,
{
    let url = format!(
        "https://api.weixin.qq.com/wxa/queryscheme?access_token={}",
        token.as_ref()
    );
    let client = Client::new();
    client
        .post(url)
        .json(req)
        .send()
        .await?
        .json::<Response<GenerateSchemeRes>>()
        .await
}
