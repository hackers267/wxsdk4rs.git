use crate::common::EnvVision;
use serde::Serialize;

#[derive(Clone, Debug, Serialize)]
pub struct JumpWxa {
    pub path: Option<String>,
    pub query: Option<String>,
    pub env_version: Option<EnvVision>,
}
