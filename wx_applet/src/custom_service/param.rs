use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Service {
    pub entity_name: Option<String>,
    pub corpid: Option<String>,
    pub bind_time: Option<u32>,
}

#[derive(Clone, Debug, Serialize)]
pub struct BindReq {
    corpid: String,
}

impl BindReq {
    pub fn new<T>(id: T) -> Self
    where
        T: AsRef<str>,
    {
        Self {
            corpid: id.as_ref().to_owned(),
        }
    }
}
#[derive(Clone, Debug, Deserialize)]
pub struct BindRes;
