use reqwest::Client;

use crate::common::{query_url, Response};

use super::param::{BindReq, BindRes, Service};

pub async fn get_service<T>(token: T) -> Result<Response<Service>, reqwest::Error>
where
    T: AsRef<str>,
{
    let url = query_url("https://api.weixin.qq.com/customservice/work/get", token);
    let client = Client::new();
    client
        .get(url)
        .send()
        .await?
        .json::<Response<Service>>()
        .await
}

pub async fn unbind<T>(token: T, req: &BindReq) -> Result<Response<BindRes>, reqwest::Error>
where
    T: AsRef<str>,
{
    let url = query_url("https://api.weixin.qq.com/customservice/work/unbind", token);
    let client = Client::new();
    client
        .post(url)
        .json(req)
        .send()
        .await?
        .json::<Response<BindRes>>()
        .await
}
pub async fn bind<T>(token: T, req: &BindReq) -> Result<Response<BindRes>, reqwest::Error>
where
    T: AsRef<str>,
{
    let url = query_url("https://api.weixin.qq.com/customservice/work/bind", token);
    let client = Client::new();
    client
        .post(url)
        .json(req)
        .send()
        .await?
        .json::<Response<BindRes>>()
        .await
}
