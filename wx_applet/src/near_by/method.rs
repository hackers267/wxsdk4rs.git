use reqwest::Client;
use reqwest::Result;

use super::param::NearbyStatusReq;
use super::param::NearbyStatusRes;
use super::param::QueryNearby;
use super::param::QueryNearbyRes;
use super::param::{DelNearbyPoiReq, DelNearbyPoiRes, NearByPoi, NearByPoiRes};
use crate::common::Response;

pub async fn add_near_by_poi<T>(token: T, req: &NearByPoi) -> Result<Response<NearByPoiRes>>
where
    T: AsRef<str>,
{
    let url = format!(
        "https://api.weixin.qq.com/wxa/addnearbypoi?access_token={}",
        token.as_ref()
    );
    let client = Client::new();
    client
        .post(url)
        .json(req)
        .send()
        .await?
        .json::<Response<NearByPoiRes>>()
        .await
}

pub async fn delete_nearby_poi<T>(
    token: T,
    req: &DelNearbyPoiReq,
) -> std::prelude::v1::Result<Response<DelNearbyPoiRes>, reqwest::Error>
where
    T: AsRef<str>,
{
    let url = format!(
        "https://api.weixin.qq.com/wxa/delnearbypoi?access_token={}",
        token.as_ref()
    );
    let client = Client::new();
    client
        .post(url)
        .json(req)
        .send()
        .await?
        .json::<Response<DelNearbyPoiRes>>()
        .await
}

pub async fn get_nearby_poi_list<T>(
    token: T,
    req: &QueryNearby,
) -> std::prelude::v1::Result<Response<QueryNearbyRes>, reqwest::Error>
where
    T: AsRef<str>,
{
    let url = format!(
        "https://api.weixin.qq.com/wxa/getnearbypoilist?access_token={}",
        token.as_ref()
    );
    let client = Client::new();
    client
        .post(url)
        .json(req)
        .send()
        .await?
        .json::<Response<QueryNearbyRes>>()
        .await
}

pub async fn set_nearby_poi_status<T>(
    token: T,
    req: &NearbyStatusReq,
) -> std::prelude::v1::Result<Response<NearbyStatusRes>, reqwest::Error>
where
    T: AsRef<str>,
{
    let url = format!(
        "https://api.weixin.qq.com/wxa/setnearbypoishowstatus?access_token={}",
        token.as_ref()
    );
    let client = Client::new();
    client
        .post(url)
        .json(req)
        .send()
        .await?
        .json::<Response<NearbyStatusRes>>()
        .await
}
