use reqwest::Client;

use crate::{
    common::{query_url, Response},
    data_analysis::param::{
        PerformanceDataReq, PerformanceDataRes, RetainReq, UserPortraitRes, VisitDistribution,
        VisitPage,
    },
};

pub async fn get_visit_page<T>(
    token: T,
    req: &RetainReq,
) -> Result<Response<VisitPage>, reqwest::Error>
where
    T: AsRef<str>,
{
    let url = query_url(
        "https://api.weixin.qq.com/datacube/getweanalysisappidvisitpage",
        token,
    );
    let client = Client::new();
    client
        .post(url)
        .json(req)
        .send()
        .await?
        .json::<Response<VisitPage>>()
        .await
}

pub async fn get_user_portrait<T>(
    token: T,
    req: &RetainReq,
) -> Result<Response<UserPortraitRes>, reqwest::Error>
where
    T: AsRef<str>,
{
    let url = query_url(
        "https://api.weixin.qq.com/datacube/getweanalysisappiduserportrait",
        token,
    );
    let client = Client::new();
    client
        .post(url)
        .json(req)
        .send()
        .await?
        .json::<Response<UserPortraitRes>>()
        .await
}

pub async fn get_visit_distribution<T>(
    token: T,
    req: &RetainReq,
) -> Result<Response<VisitDistribution>, reqwest::Error>
where
    T: AsRef<str>,
{
    let url = query_url(
        "https://api.weixin.qq.com/datacube/getweanalysisappidvisitdistribution",
        token,
    );
    let client = Client::new();
    client
        .post(url)
        .json(req)
        .send()
        .await?
        .json::<Response<VisitDistribution>>()
        .await
}

pub async fn get_performance_data<T>(
    token: T,
    req: &PerformanceDataReq,
) -> Result<Response<PerformanceDataRes>, reqwest::Error>
where
    T: AsRef<str>,
{
    let url = query_url(
        "https://api.weixin.qq.com/wxa/business/performance/boot",
        token,
    );
    let client = Client::new();
    client
        .post(url)
        .json(req)
        .send()
        .await?
        .json::<Response<PerformanceDataRes>>()
        .await
}
