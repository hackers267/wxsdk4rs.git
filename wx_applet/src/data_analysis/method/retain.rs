use reqwest::Client;

use crate::common::{query_url, Response};
use crate::data_analysis::param::{RetainReq, RetainRes};

pub async fn get_weekly_retain<T>(
    token: T,
    req: &RetainReq,
) -> Result<Response<RetainRes>, reqwest::Error>
where
    T: AsRef<str>,
{
    let url = "https://api.weixin.qq.com/datacube/getweanalysisappidweeklyretaininfo";
    get_retain(token, req, url).await
}

pub async fn get_monthly_retain<T>(
    token: T,
    req: &RetainReq,
) -> Result<Response<RetainRes>, reqwest::Error>
where
    T: AsRef<str>,
{
    let url = "https://api.weixin.qq.com/datacube/getweanalysisappidmonthlyretaininfo";
    get_retain(token, req, url).await
}

pub async fn get_daily_retain<T>(
    token: T,
    req: &RetainReq,
) -> Result<Response<RetainRes>, reqwest::Error>
where
    T: AsRef<str>,
{
    let url = "https://api.weixin.qq.com/datacube/getweanalysisappiddailyretaininfo";
    get_retain(token, req, url).await
}
async fn get_retain<T, U>(
    token: T,
    req: &RetainReq,
    url: U,
) -> Result<Response<RetainRes>, reqwest::Error>
where
    T: AsRef<str>,
    U: AsRef<str>,
{
    let url = query_url(url, token);
    let client = Client::new();
    client
        .post(url)
        .json(req)
        .send()
        .await?
        .json::<Response<RetainRes>>()
        .await
}
