use serde::Serialize;

use crate::qr_code::{rgb::Rgb, width::Width};

#[derive(Clone, Debug, Serialize, Default)]
pub struct QrCodeReq {
    path: String,
    width: Option<Width>,
    auto_color: Option<bool>,
    line_color: Option<Rgb>,
    is_hyaline: Option<bool>,
    env_version: Option<String>,
}

impl QrCodeReq {
    pub fn new<T>(path: T) -> Self
    where
        T: AsRef<str>,
    {
        Self {
            path: path.as_ref().to_owned(),
            ..Default::default()
        }
    }
}

impl QrCodeReq {
    pub fn builder() -> QrCodeReqBuilder {
        QrCodeReqBuilder::default()
    }
}

#[derive(Clone, Debug, Default)]
pub struct QrCodeReqBuilder {
    path: String,
    width: Option<Width>,
    auto_color: Option<bool>,
    line_color: Option<Rgb>,
    is_hyaline: Option<bool>,
    env_version: Option<String>,
}

impl QrCodeReqBuilder {
    pub fn path<T>(mut self, path: T) -> Self
    where
        T: AsRef<str>,
    {
        self.path = path.as_ref().to_owned();
        self
    }

    pub fn width(mut self, width: &Width) -> Self {
        self.width = Some(width.to_owned());
        self
    }

    pub fn auto_color(mut self, auto_color: bool) -> Self {
        self.auto_color = Some(auto_color);
        self
    }

    pub fn line_color(mut self, line_color: &Rgb) -> Self {
        self.line_color = Some(line_color.to_owned());
        self
    }
    pub fn hyaline(mut self, hyaline: bool) -> Self {
        self.is_hyaline = Some(hyaline);
        self
    }

    pub fn env_version<T>(mut self, env_version: T) -> Self
    where
        T: AsRef<str>,
    {
        self.env_version = Some(env_version.as_ref().to_owned());
        self
    }

    pub fn build(self) -> QrCodeReq {
        QrCodeReq {
            path: self.path,
            width: self.width,
            auto_color: self.auto_color,
            line_color: self.line_color,
            is_hyaline: self.is_hyaline,
            env_version: self.env_version,
        }
    }
}
