use serde::Serialize;

use crate::qr_code::{rgb::Rgb, width::Width};

#[derive(Clone, Debug, Serialize, Default)]
pub struct UnlimitReq {
    auto_color: Option<bool>,
    check_path: Option<bool>,
    env_version: Option<String>,
    is_hyaline: Option<bool>,
    line_color: Option<Rgb>,
    page: Option<String>,
    scene: String,
    width: Option<Width>,
}

impl UnlimitReq {
    pub fn new<T>(scene: T) -> Self
    where
        T: AsRef<str>,
    {
        Self {
            scene: scene.as_ref().to_owned(),
            ..Default::default()
        }
    }
}

impl UnlimitReq {
    pub fn builder() -> UnlimitReqBuilder {
        UnlimitReqBuilder::default()
    }
}

#[derive(Clone, Debug, Default)]
pub struct UnlimitReqBuilder {
    scene: String,
    page: Option<String>,
    check_path: Option<bool>,
    env_version: Option<String>,
    width: Option<Width>,
    auto_color: Option<bool>,
    line_color: Option<Rgb>,
    is_hyaline: Option<bool>,
}

impl UnlimitReqBuilder {
    pub fn scene<T>(mut self, scene: T) -> Self
    where
        T: AsRef<str>,
    {
        self.scene = scene.as_ref().to_owned();
        self
    }
    pub fn page<T>(mut self, page: T) -> Self
    where
        T: AsRef<str>,
    {
        self.page = Some(page.as_ref().to_owned());
        self
    }

    pub fn check_path(mut self, check_path: bool) -> Self {
        self.check_path = Some(check_path);
        self
    }

    pub fn env_version<T>(mut self, env_version: T) -> Self
    where
        T: AsRef<str>,
    {
        self.env_version = Some(env_version.as_ref().to_owned());
        self
    }
    pub fn width(mut self, width: &Width) -> Self {
        self.width = Some(width.clone());
        self
    }

    pub fn auto_color(mut self, auto_color: bool) -> Self {
        self.auto_color = Some(auto_color);
        self
    }

    pub fn line_color(mut self, line_color: &Rgb) -> Self {
        self.line_color = Some(line_color.to_owned());
        self
    }

    pub fn hyaline(mut self, is_hyaline: bool) -> Self {
        self.is_hyaline = Some(is_hyaline);
        self
    }

    pub fn build(self) -> UnlimitReq {
        UnlimitReq {
            scene: self.scene,
            page: self.page,
            check_path: self.check_path,
            env_version: self.env_version,
            width: self.width,
            auto_color: self.auto_color,
            line_color: self.line_color,
            is_hyaline: self.is_hyaline,
        }
    }
}
