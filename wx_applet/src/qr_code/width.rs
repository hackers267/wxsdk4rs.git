use serde::Serialize;

#[derive(Clone, Debug, Serialize)]
pub struct Width {
    value: u16,
}

impl Width {
    pub fn new(value: u16) -> Self {
        if value >= 280 || value <= 1280 {
            Self { value }
        } else {
            panic!("最小值为280,最大值为1280，请检查你的输入")
        }
    }
}
#[macro_export]
macro_rules! qrcode_width {
    ($v:literal) => {
        $crate::qr_code::qr_code::Width::new($v)
    };
}
