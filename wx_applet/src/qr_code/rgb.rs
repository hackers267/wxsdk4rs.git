use serde::Serialize;

#[derive(Clone, Debug, Serialize)]
pub struct Rgb {
    r: u8,
    g: u8,
    b: u8,
}

impl Rgb {
    pub fn new(r: u8, g: u8, b: u8) -> Self {
        Self { r, g, b }
    }
}

#[macro_export]
macro_rules! rgb {
    ($a:literal,$b:literal,$c:literal) => {
        $crate::qr_code::qr_code::rgb::Rgb::new($a, $b, $c)
    };
}
