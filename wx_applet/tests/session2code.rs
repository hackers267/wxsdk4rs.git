use std::env;

use anyhow::Result;
use wx_applet::auth::code2session::{code2session, Code2Seccion};

#[tokio::test]
async fn main() -> Result<()> {
    dotenvy::dotenv()?;
    let app_id = env::var("APPID").expect("Can't find the app_id");
    let secret = env::var("SECRET").expect("Can't find the secret");
    let code = env::var("CODE").expect("Can't find the code");
    let session = Code2Seccion::new(app_id, secret, code.to_owned());
    let result = code2session(&session).await;
    println!("Result is:{:?}", result);

    Ok(())
}
