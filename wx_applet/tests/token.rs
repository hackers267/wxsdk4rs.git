use anyhow::Result;
use core::panic;
use std::env;
use wx_applet::{
    common::Response,
    token::{get_token, TokenParams},
};

#[tokio::test]
async fn main() -> Result<()> {
    dotenvy::dotenv()?;
    let app_id = env::var("APPID").expect("Can't find the app_id");
    let secret = env::var("SECRET").expect("Can't find the secret");
    let param = TokenParams::new(app_id, secret);
    let result = get_token(&param).await;
    let result = result.unwrap();
    eprintln!("{:?}", result);
    match result {
        Response::Ok(token) => {
            assert!(!token.access_token.is_empty());
            assert_eq!(token.expires_in, 7200);
        }
        _ => panic!("Error"),
    };
    Ok(())
}
